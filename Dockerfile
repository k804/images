FROM centos AS builder
RUN yum install wget make expat-devel gcc pcre-devel openssl-devel -y
RUN mkdir sources
RUN useradd -r apache
ENV HTTPD_HOME=/etc/apache2
WORKDIR /root/sources
RUN wget -qO- https://downloads.apache.org//httpd/httpd-2.4.43.tar.gz | tar -xz && \
    wget -qO- https://downloads.apache.org//apr/apr-1.7.0.tar.gz | tar -xz && \
    wget -qO- https://downloads.apache.org//apr/apr-util-1.6.1.tar.gz | tar -xz && \
    wget -qO- https://github.com/libexpat/libexpat/releases/download/R_2_2_7/expat-2.2.7.tar.gz | tar -xz

RUN cp -r apr-1.7.0 httpd-2.4.43/srclib/apr && \
    cp -r apr-util-1.6.1 httpd-2.4.43/srclib/apr-util

WORKDIR /root/sources/httpd-2.4.43
RUN ./configure --prefix=$HTTPD_HOME --enable-ssl --enable-so --with-included-apr --with-mpm=event && \
     make && \
     make install
WORKDIR /etc/apache2/bin

RUN chmod +x apachectl
RUN sed -i -e '/^User/ c User apache' /etc/apache2/conf/httpd.conf
RUN sed -i -e '/^Group/ c Group apache' /etc/apache2/conf/httpd.conf
RUN chown -R apache:apache /etc/apache2/
WORKDIR /etc/apache2/htdocs
#USER apache
RUN rm -rf /root/sources/



FROM centos
RUN mkdir /etc/apache2
RUN useradd -r apache
COPY start.sh /usr/bin/apacheStart
RUN chmod +x /usr/bin/apacheStart
WORKDIR /etc/apache2/htdocs
COPY --from=builder /etc/apache2 /etc/apache2
ENTRYPOINT apacheStart